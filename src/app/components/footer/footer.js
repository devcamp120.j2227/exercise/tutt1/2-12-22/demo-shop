const FooterForm = () => {
    return (
        <div className="container-fluid footer text-light">
            <div className="container">
                <div className="row padding">
                    <div className="col-sm-4">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <p>©  2018 . All Rights Reserved.</p>
                    </div>
                    <div className="col-sm-4">
                        <h5>Contacts</h5>
                        <b>Address:</b>
                        <p>Kolkata, West Bengal, India</p>
                        <b>Email:</b>
                        <p><a href="#" className="link">info@example.com</a></p>
                        <b>Phones:</b>
                        <p><a href="#" className="link">+91 99999999</a> or <a href="#" className="link">+91 11111111</a></p>
                    </div>
                    <div className="col-sm-4">
                        <h5 style={{marginLeft: "32px"}}>Links</h5>
                        <ul className="link-list">
                            <li className="margin-top-li"><a className="link link-hover" href="#">About</a></li>
                            <li className="margin-top-li"><a className="link link-hover" href="#">Project</a></li>
                            <li className="margin-top-li"><a className="link link-hover" href="#">Blog</a></li>
                            <li className="margin-top-li"><a className="link link-hover" href="#">Contacts</a></li>
                            <li className="margin-top-li"><a className="link link-hover" href="#">Pricing</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="row text-center">
                <div className="col border footer-app">
                    <b href="#" className="link">FACEBOOK</b>
                </div>
                <div className="col border footer-app">
                    <b href="#" className="link">INSTAGRAM</b>
                </div>
                <div className="col border footer-app">
                    <b href="#" className="link">TWITTER</b>
                </div>
                <div className="col border footer-app">
                    <b href="#" className="link">GOOGLE</b>
                </div>
            </div>
        </div>
    )
}

export default FooterForm