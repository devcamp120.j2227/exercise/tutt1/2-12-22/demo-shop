import productData from "./productData";

const ContentForm = () => {
    const limitProduct = productData.Products.slice(0, 9);
    return (
        <div className="container" style={{paddingLeft: "100px", marginTop: "50px", marginBottom: "100px"}}>
            <div className="row">
                <h3>Product List</h3>
                <p>Showing 1 - 9 of 24 products</p>
            </div>
            <div className="div-product">
                {limitProduct.map((product, index) => (
                    <div class="card">
                        <h5 class="card-header text-primary text-center" style={{paddingTop: "20px", paddingBottom: "20px"}}>{product.Title}</h5>
                        <div class="card-body text-center">
                            <img className="image" src={product.ImageUrl}/>
                            <p class="card-title text">{product.Description}</p>
                            <p style={{textAlign: "left"}}><b>Category: </b>{product.Category}</p>
                            <p style={{textAlign: "left"}}><b>Made by: </b>{product.Manufacturer}</p>
                            <p style={{textAlign: "left"}}><b>Organic: </b>{product.Organic == true ? "Yes" : "No"}</p>
                            <p style={{textAlign: "left"}}><b>Price: </b>{product.Price}$</p>
                            <a href="#" class="btn btn-primary">Add to Cart</a>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    )
}

export default ContentForm