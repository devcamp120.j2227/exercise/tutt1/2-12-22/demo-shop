import HeaderForm from "./app/components/header/header";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import FooterForm from "./app/components/footer/footer";
import ContentForm from "./app/components/content/content";

function App() {
  return (
    <div>
      <HeaderForm/>
      <ContentForm/>
      <FooterForm/>
    </div>
  );
}

export default App;
